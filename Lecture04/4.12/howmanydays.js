monthAsked = 5;

if (monthAsked == 1) {
  console.log("January has 31 days.");
} else if (monthAsked === 2) {
  console.log("February has 28 days.");
} else if (monthAsked === 3) {
  console.log("March has 31 days.");
} else if (monthAsked === 4) {
  console.log("April has 30 days.");
} else if (monthAsked === 5) {
  console.log("May has 31 days.");
} else if (monthAsked === 6) {
  console.log("June has 30 days.");
} else if (monthAsked === 7) {
  console.log("July has 31 days.");
} else if (monthAsked === 8) {
  console.log("August has 31 days.");
} else if (monthAsked === 9) {
  console.log("September has 30 days.");
} else if (monthAsked === 10) {
  console.log("October has 31 days.");
} else if (monthAsked === 11) {
  console.log("November has 30 days.");
} else if (monthAsked === 12) {
  console.log("December has 31 days.");
}
