const number1 = 27;
const number2 = 22;
const number3 = 29;
let smallest = +Infinity;
let largest = -Infinity;

if (number1 == number2 && number1 == number3) {
  console.log("All numbers are equal.");
  return;
}

if (number1 > largest) {
  largest = number1;
}
if (number2 > largest) {
  largest = number2;
}
if (number3 > largest) {
  largest = number3;
}
console.log(largest, "is the largest number.");

if (number1 < smallest) {
  smallest = number1;
}
if (number2 < smallest) {
  smallest = number2;
}
if (number3 < smallest) {
  smallest = number3;
}

console.log(smallest, "is the smallest number.");
