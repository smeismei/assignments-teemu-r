const balance = 250;
const isActive = true;
const checkBalance = true;

if (checkBalance === true && isActive === true && balance > 0) {
  console.log("Your account balance is", balance, "dollars.");
}
if (checkBalance === true && isActive === true && balance <= 0) {
  console.log("Your balance is negative.");
}
if (checkBalance === true && isActive === false) {
  console.log("Your account is not active.");
}
if (checkBalance === false) {
  console.log("Have a nice day!");
}
