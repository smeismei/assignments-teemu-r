const firstWord = "length";
const secondWord = "string";

if (firstWord.length > secondWord.length) {
  console.log(firstWord, " is longer than ", secondWord);
} else if (secondWord.length > firstWord.length) {
  console.log(secondWord, " is longer than ", firstWord);
} else {
  console.log("The words are of equal length");
}
