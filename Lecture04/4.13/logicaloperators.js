const score = 4;
const hoursPlayed = 20;
const price = 5;

if (
  (score === 4 && price === 0) ||
  (score >= 4 && hoursPlayed / price >= 4) ||
  (score >= 5 && hoursPlayed / price >= 2)
) {
  console.log("The game is worth it :D");
} else {
  console.log("The game is NOT worth it.");
}
