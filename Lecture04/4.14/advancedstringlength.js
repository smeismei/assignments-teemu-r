const firstName = "Lilja";
const secondName = "Oliveri";
const thirdName = "Teemu";

let longest = firstName;
let shortest = firstName;
let middle = firstName;

if (longest.length < secondName.length) {
  longest = secondName;
}
if (longest.length < thirdName.length) {
  longest = thirdName;
} //find longest name
if (shortest.length > secondName.length) {
  shortest = secondName;
}
if (shortest.length > thirdName.length) {
  shortest = thirdName;
} //find shortest name
if (firstName !== longest && firstName !== shortest) {
  middle = firstName;
}
if (secondName !== longest && secondName !== shortest) {
  middle = secondName;
}
if (thirdName !== longest && thirdName !== shortest) {
  middle = thirdName;
} //find middle name through process of elimination

console.log(longest, middle, shortest);
