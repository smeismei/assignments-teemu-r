const numbers = [5, 13, 2, 10, 8];

function countProduct(array) {
  let total = 1;
  array.forEach((num) => {
    total *= num;
  });
  console.log(total);
}

function countAverage(array) {
  let total = 0;
  array.forEach((num) => {
    total += num;
  });
  total = total / array.length;
  console.log(total);
}

countProduct(numbers);
countAverage(numbers);
