const animals = ["horse", "cow", "dog", "hamster", "donkey", "cat", "parrot"];

const Oanimal = animals.filter((item) => {
  return item.includes("o");
});

console.log(
  "including all O names, the party is joined by",
  Oanimal.toString()
);

const discriminationFilter = animals.filter((item) => {
  return !item.includes("o") && !item.includes("h");
});

console.log(
  "after the culling, the only one left is..",
  discriminationFilter.toString()
);
