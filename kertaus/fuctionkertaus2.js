function randomGen(min, max) {
  const minInt = Math.floor(min);
  const maxInt = Math.ceil(max);
  return Math.floor(Math.random() * (maxInt - minInt) + 1 + minInt);
}

for (let i = 0; i < 20; i++) {
  console.log(randomGen(5, 20));
}
