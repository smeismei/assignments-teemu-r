const animals = ["horse", "cow", "dog", "hamster", "donkey", "cat", "parrot"];

function tAnimal() {
  const tAnimal = animals.find((item) => {
    return item.charAt(item.length - 1) == "t";
  });

  console.log(tAnimal);
}

function dAndYAnimal() {
  const dyAnimal = animals.find((item) => {
    return item.charAt(0) == "d" && item.charAt(item.length - 1) == "y";
  });

  console.log(dyAnimal);
}

tAnimal();
dAndYAnimal();
