const animals = ["horse", "cow", "dog", "hamster", "donkey", "cat", "parrot"];

const animalLengths = animals.map((item) => {
  return item.length;
});

console.log(animalLengths);

const discriminatedAnimals = animals.map((item) => {
  return item.charAt(1) === "o";
});

console.log(discriminatedAnimals);
