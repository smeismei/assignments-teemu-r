const arr = ["the", "quick", "brown", "fox"];
console.log(arr);
console.log(arr[1], arr[2]);
arr[2] = "gray";
console.log(arr);
arr.push("over", "lazy", "dog");
console.log(arr);
arr.unshift("pangram:");
console.log(arr);
