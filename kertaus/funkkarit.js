const list = [
  "tomaatti",
  "peruna",
  "lanttu",
  "porkkana",
  "pentti",
  "korttipakka",
];

list.forEach((item, i) => {
  console.log(
    "parameter function was called with parameter",
    item,
    "which has the index of",
    i
  );
});
