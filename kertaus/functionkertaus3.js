function namedFunction(num1, num2, num3) {
  return num1 + num2 + num3;
}

const num1 = 1;
const num2 = 2;
const num3 = 3;
(num1, num2, num3) => {
  console.log("anon ", num1 + num2 + num3);
};

const arrowFunction = (num1, num2, num3) => {
  return num1 + num2 + num3;
};

console.log("named: ", namedFunction(1, 2, 3));
console.log("arrow: ", arrowFunction(1, 2, 3));
