const upperlever = true;
const lowerlever = true;

if ((upperlever || lowerlever) && !(upperlever && lowerlever)) {
  console.log("The gate works well, power flows.");
} else if (upperlever && lowerlever) {
  console.log("Too much power! Abort!!");
} else {
  console.log("No power flows.");
}
