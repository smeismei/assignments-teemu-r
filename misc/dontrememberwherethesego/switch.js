const language = "fi";

switch (language) {
  case "fi":
    console.log("Tervetuloa");
    break;

  case "en":
    console.log("Welcome");
    break;

  case "se":
    console.log("Välkommen");
    break;

  default:
    console.log("Unknown language");
}
