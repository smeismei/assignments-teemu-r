let setTimeoutWrapper = function (arg, func) {
  setTimeout(() => {
    func();
  }, arg);
};
let timer = 1000;

while (true) {
  setTimeoutWrapper(timer, () => {
    console.log("a second has passed.");
    timer += 1000;
    console.log(timer);
  });
}
