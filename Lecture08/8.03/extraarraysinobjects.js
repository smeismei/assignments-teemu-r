const student = {
  name: "Aili",
  credits: 45,
  courseGrades: {
    "Intro to Programming": 4,
    "JavaScript Basics": 3,
    "Functional Programming": 5,
  },
};

console.log(student);

delete student.courseGrades;
student.courses = [];
function addCourse(courseName, courseGrade) {
  student.courses.push({
    name: courseName,
    grade: courseGrade,
  });
}

addCourse("Intro to Programming", 3);
addCourse("JavaScript Basics", 3);
addCourse("Functional Programming", 5);
console.log(student);
