const student = {
  name: "Aili",
  credits: 45,
  courses: [
    {
      name: "Intro to Programming",
      grade: 3,
    },
    {
      name: "JavaScript Basics",
      grade: 3,
    },
    {
      name: "Functional Programming",
      grade: 5,
    },
  ],
};

function addCourse(courseName, courseGrade) {
  student.courses.push({
    name: courseName,
    grade: courseGrade,
  });
}

console.log(
  student.name +
    " got " +
    student.courses[0].grade +
    " from " +
    student.courses[0].name
);

addCourse("Python Basics", 4);

console.log(student.courses);
