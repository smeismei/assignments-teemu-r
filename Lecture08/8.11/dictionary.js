const translations = {
  hello: "hei",
  world: "maailma",
  bit: "bitti",
  byte: "tavu",
  integer: "kokonaisluku",
  boolean: "totuusarvo",
  string: "merkkijono",
  network: "verkko",
};

function printTranslatableWords() {
  console.log(Object.keys(translations));
}

function translateWord(str) {
  if (str in translations) {
    return translations[str];
  } else {
    console.log("No translation exists for word given as argument!");
    return null;
  }
}

printTranslatableWords();
console.log(translateWord("bit"));
console.log(translateWord("bittiavaruus"));
