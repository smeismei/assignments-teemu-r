class Rectangle {
  constructor(height, width) {
    this.height = height;
    this.width = width;
  }
  getArea() {
    return this.width * this.height;
  }
}

const rectangle1 = new Rectangle(24, 55);
const rectangle2 = new Rectangle(44, 94);
const rectangle3 = new Rectangle(52, 86);
const rectangle4 = new Rectangle(77, 9);

console.log(rectangle1);
console.log("area is", rectangle1.getArea());
console.log(rectangle2);
console.log("area is", rectangle2.getArea());
console.log(rectangle3);
console.log("area is", rectangle3.getArea());
console.log(rectangle4);
console.log("area is", rectangle4.getArea());
