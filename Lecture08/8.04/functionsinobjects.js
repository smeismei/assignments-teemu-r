const calculator = {
  name: "Buutti SuperCalculator 6000",
  cache: "96 GB",
  clockSpeed: 9001.0,
  overclock: function () {
    this.clockSpeed += 500;
  },
  savePower: function () {
    if (this.clockSpeed <= 2000) {
      this.clockSpeed = this.clockSpeed / 2;
    } else {
      this.clockSpeed = 2000;
    }
  },
};
console.log(calculator.clockSpeed);
calculator.overclock();
console.log(calculator.clockSpeed);
calculator.savePower();
console.log(calculator.clockSpeed);
calculator.savePower();
console.log(calculator.clockSpeed);
