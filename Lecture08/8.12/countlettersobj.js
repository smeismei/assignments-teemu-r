function getCountOfLetters(str) {
  const letterObj = {};
  for (let i = 0; i < str.length; i++) {
    let character = str.charAt(i);
    if (!letterObj[character]) {
      letterObj[character] = 1;
    } else {
      letterObj[character] += 1;
    }
  }
  console.log(letterObj);
}

getCountOfLetters("the quick brown fox is no more");
