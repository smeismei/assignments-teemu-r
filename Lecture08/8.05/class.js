class Rectangle {
  constructor(height, width) {
    this.height = height;
    this.width = width;
  }
}

const rectangle1 = new Rectangle(24, 55);
const rectangle2 = new Rectangle(44, 94);
const rectangle3 = new Rectangle(52, 86);
const rectangle4 = new Rectangle(77, 9);

console.log(rectangle1);
console.log(rectangle2);
console.log(rectangle3);
console.log(rectangle4);
