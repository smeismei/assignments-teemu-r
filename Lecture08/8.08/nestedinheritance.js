class Shape {
  constructor(width, height) {
    this.width = width;
    this.height = height;
  }
  getArea() {
    return 0;
  }
}
class Rectangle extends Shape {
  constructor(width, height) {
    super(width, height);
  }
  getArea() {
    return this.width * this.height;
  }
}
class Square extends Rectangle {
  constructor(width) {
    super(width);
  }
  getArea() {
    return this.width * this.width;
  }
}
class Ellipse extends Shape {
  constructor(width, height) {
    super(width, height);
  }
  getArea() {
    return (((Math.PI * this.width) / 2) * this.height) / 2;
  }
}
class Circle extends Ellipse {
  constructor(width) {
    super(width);
  }
  getArea() {
    return Math.PI * (this.width * this.width);
  }
}
class Triangle extends Shape {
  constructor(width, height) {
    super(width, height);
  }
  getArea() {
    return (this.width * this.height) / 2;
  }
}
const rectangle1 = new Rectangle(40, 30).getArea();
const ellipse1 = new Ellipse(40, 30).getArea();
const triangle1 = new Triangle(40, 30).getArea();
const square1 = new Square(20).getArea();
const circle1 = new Circle(40).getArea();
console.log(rectangle1);
console.log(ellipse1);
console.log(triangle1);
console.log(square1);
console.log(circle1);
