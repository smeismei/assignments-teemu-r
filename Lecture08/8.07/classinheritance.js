class Shape {
  constructor(width, height) {
    this.width = width;
    this.height = height;
  }
  getArea() {
    return 0;
  }
}
class Rectangle extends Shape {
  constructor(width, height) {
    super(width, height);
  }
  getArea() {
    return this.width * this.height;
  }
}
class Ellipse extends Shape {
  constructor(width, height) {
    super(width, height);
  }
  getArea() {
    return (((Math.PI * this.width) / 2) * this.height) / 2;
  }
}
class Triangle extends Shape {
  constructor(width, height) {
    super(width, height);
  }
  getArea() {
    return (this.width * this.height) / 2;
  }
}
const rectangle1 = new Rectangle(40, 30).getArea();
const ellipse1 = new Ellipse(40, 30).getArea();
const triangle1 = new Triangle(40, 30).getArea();
console.log(rectangle1);
console.log(ellipse1);
console.log(triangle1);
