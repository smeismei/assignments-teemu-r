const fruits = {
  banana: 118,
  apple: 85,
  mango: 200,
  lemon: 65,
};

function printWeight(fruit) {
  if (fruit in fruits) {
    console.log(fruit + " weighs " + fruits[fruit] + " grams.");
  } else {
    console.log("No such fruit in my basket! Here's everything I have..");
    console.log(Object.keys(fruits));
  }
}
printWeight("mango");
printWeight("My mango is to blow up");

fruits.dragonfruit = 50;
fruits.kiwi = 300;
fruits.orange = 130;

printWeight("dragonfruit");
printWeight("kiwi");
