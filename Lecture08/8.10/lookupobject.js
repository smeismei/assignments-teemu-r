const scores = {
  S: 8,
  A: 6,
  B: 4,
  C: 3,
  D: 2,
  F: 0,
};
function calculateTotalScore(str) {
  let total = 0;
  for (let i = 0; i < str.length; i++) {
    total += scores[str.charAt(i)];
  }
  return total;
}
function calculateAverageScore(str) {
  return calculateTotalScore(str) / str.length;
}

let totalScore = calculateTotalScore("SABCDFDDFCBAS");
console.log(totalScore);
let totalScoreAverage = calculateAverageScore("SABCDFDDFCBAS");
console.log(totalScoreAverage);

const gradeSequence = ["AABAACAA", "FFDFDCCDCB", "ACBSABA", "CCDFABABC"];
const mappedSequence = gradeSequence.map(calculateAverageScore);
console.log(mappedSequence);
