import fs from "fs";

let frankenstein = fs.readFileSync("./frankenstein.txt", "utf8");
let words = frankenstein.toLowerCase();
let frankenRay = words.split(" ");
let wordCount = 0;

for (let i = 0; i < frankenRay.length; i++)
  if (frankenRay[i].includes("monster")) {
    frankenRay[i] = "puppy";
    wordCount++;
    console.log(frankenRay[i]);
  }

fs.writeFileSync("./frankenstein.txt", frankenRay.toString(), "utf8");
console.log(wordCount);
