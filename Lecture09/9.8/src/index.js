const wait = (delay) => {
  const t = Date.now() + delay;
  while (Date.now() < t) {}
};

while (true) {
  console.log("");
  console.log("-----");
  wait(500);
  console.clear();
  console.log(" \\");
  console.log("  \\");
  console.log("   \\");
  wait(500);
  console.clear();
  console.log("  |");
  console.log("  |");
  console.log("  |");
  wait(500);
  console.clear();
  console.log("   /");
  console.log("  /");
  console.log(" /");
  wait(500);
  console.clear();
}
