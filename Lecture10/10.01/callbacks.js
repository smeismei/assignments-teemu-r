let setTimeoutWrapper = function (arg, func) {
  setTimeout(() => {
    func();
  }, arg);
};

console.log("3...");
setTimeoutWrapper(1000, () => {
  console.log("2...");
  setTimeoutWrapper(1000, () => {
    console.log("1...");
    setTimeoutWrapper(1000, () => {
      console.log("GO!");
    });
  });
});
