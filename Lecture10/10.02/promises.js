console.log("3...");
let promise = new Promise((yes, no) => {
  setTimeout(() => {
    yes("2...");
  }, 1000);
}).then((result) => {
  console.log(result);
  return new Promise((yes, no) => {
    setTimeout(() => {
      yes("1...");
    }, 1000);
  }).then((result) => {
    console.log(result);
    return new Promise((yes, no) => {
      setTimeout(() => {
        yes("GO!");
      }, 1000);
    }).then((result) => {
      console.log(result);
    });
  });
});
