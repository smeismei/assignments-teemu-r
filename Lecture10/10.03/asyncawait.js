let timer = (time, str) =>
  new Promise((yes, no) => {
    setTimeout(() => yes(str), time);
  });

let async = async () => {
  console.log("3...");
  console.log(await timer(1000, "2..."));
  console.log(await timer(1000, "1..."));
  console.log(await timer(1000, "GO!"));
};
async();
