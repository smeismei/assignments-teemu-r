import readline from "readline";
import fs from "fs";

const reader = readline.createInterface(process.stdin, process.stdout);
let timer = (time, str) =>
  new Promise((yes, no) => {
    setTimeout(() => yes(str), time);
  });

let async = async () => {
  console.log(await timer(1000, "3..."));
  console.log(await timer(1000, "2..."));
  console.log(await timer(1000, "1..."));
  console.log(await timer(1000, fs.readFileSync("./src/youdied.txt", "ascii")));
};

reader.question("What is the super secret password? \n", (answer) => {
  if (answer.toLowerCase() == "elephant") {
    console.log(fs.readFileSync("./src/elephant.txt", "ascii"));
    reader.close();
  } else {
    console.error("Access to confidential files denied, obliterating user in:");
    async();
    reader.close();
  }
});
