import readline from "readline/promises";

const reader = readline.createInterface(process.stdin, process.stdout);

function randomFromRange(min, max) {
  const y = Math.random() * (max - min + 1) + min;
  const z = Math.floor(y);
  return z;
}

async function sumGame() {
  let correctAnswer = 0;

  for (let i = 0; true; i++) {
    let num1 = randomFromRange(1, 10);
    let num2 = randomFromRange(1, 10);
    correctAnswer = num1 + num2;

    let userAnswer = await reader.question(
      "What is the sum of " + num1 + " + " + num2 + ": "
    );

    if (Number(userAnswer) === correctAnswer) {
      console.log("Correct!");
    } else {
      console.log("Wrong. Game over!");
      break;
    }
  }
  reader.close();
}

sumGame();
