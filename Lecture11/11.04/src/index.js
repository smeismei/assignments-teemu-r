import axios from "axios";
import fs from "fs";

const elephant = fs.readFileSync("./src/elephant.txt", "ascii");

const response = await axios.post(
  "https://buutti-messages-api.azurewebsites.net/api/messages",
  { user: "TR", text: "ei anna :(", id: 1 }
);
if (response.status === 201) {
  console.log("Post request successful!");
} else {
  console.log("Something went wrong!!");
}
console.log("Server responded with", response.data);
console.log(response.status);
