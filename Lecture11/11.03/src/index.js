import axios from "axios";

const messages = await axios.get(
  "https://buutti-messages-api.azurewebsites.net/api/messages"
);
const named = await axios.get(
  "https://buutti-messages-api.azurewebsites.net/api/messages?user=TR"
);

function messagePrinter(data, arg) {
  const dataset = data.data;
  const idList = [];
  for (let i = 0; i < dataset.length; i++) {
    idList.push(dataset[i]["id"]);
  }
  for (let i = 0; i < dataset.length; i++) {
    console.log(dataset[i][arg]);
  }
  console.log(idList);
}

messagePrinter(named, "text");
