import axios from "axios";

const url = "https://buutti-messages-api.azurewebsites.net/api/messages/";
const id = 418;
const name = "TR"; //change this to switch user to delete all messages from.
const idData = await axios.get(url + "?user=" + name);

async function messageDeleter() {
  const response = await axios.delete(url + id);
  if (response.status === 200) {
    console.log("Post request successful!");
  } else {
    console.log("Something went wrong!!");
  }
  console.log("Server responded with", response.data);
  console.log(response.status);
}

// attempt at mass deleter by user, not tested.
// user discretion is advised!
async function massDeleter(data) {
  const dataset = data.data;
  const idList = [];
  for (let i = 0; i < dataset.length; i++) {
    idList.push(dataset[i]["id"]);
  }
  console.log(idList);
  for (let i = 0; i < idList.length; i++) {
    axios.delete(url + idList[i]);
  }
}

messageDeleter();
// massDeleter(idData);
// Commented to avoid deleting everything while testing.
