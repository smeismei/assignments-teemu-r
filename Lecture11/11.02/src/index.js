import axios from "axios";

const joke = await axios.get("https://api.chucknorris.io/jokes/random");
console.log(joke.data["value"], "\n");
console.log("Above joke was fetched with Axios. \n");
const fetchJoke = await fetch("https://api.chucknorris.io/jokes/random");
const fetchJokeData = await fetchJoke.json();
console.log(fetchJokeData["value"], "\n");
console.log("Above joke was fetched with Node's built in 'fetch' feature. \n");

async function searchJoke(query) {
  const joke = await axios.get(
    "https://api.chucknorris.io/jokes/search?query=" + query
  );
  return joke.data;
}

function printJokes(data) {
  function printer(data) {
    console.log(data["value"]);
  }
  data.forEach(printer);
}

const jokeData = await searchJoke("among");
printJokes(jokeData);
