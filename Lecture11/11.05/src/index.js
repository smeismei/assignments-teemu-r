import axios from "axios";

const url = "https://buutti-messages-api.azurewebsites.net/api/messages/";
const id = 418;
const body = {
  user: "TR",
  text: "patching...",
};

const response = await axios.patch(url + id, body);
if (response.status === 200) {
  console.log("Post request successful!");
} else {
  console.log("Something went wrong!!");
}
console.log("Server responded with", response.data);
console.log(response.status);
