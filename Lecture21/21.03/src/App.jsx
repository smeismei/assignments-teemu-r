import { useState } from "react";
import { useEffect } from "react";
import axios from "axios";

const App = () => {
  const [messages, setMessages] = useState([]);
  const url = "https://buutti-messages-api.azurewebsites.net/api/messages";
  console.log(messages);
  useEffect(() => {
    const fetchData = async () => {
      const response = await axios.get(url);
      const messageData = response.data;
      setMessages(messageData);
      console.log("MESSAGEDATA: ", messageData);
    };
    fetchData();
  }, []);

  return (
    <div className="App">
      <div>
        <fieldset>
          <input type="text" />
        </fieldset>
      </div>
      <div>
        {messages.map((message) => {
          return (
            <div key={message.id}>
              {message.text}
              <br />
              -- {message.user}
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default App;
