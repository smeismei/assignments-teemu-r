import { useState } from "react";
import { useEffect } from "react";

function App() {
  const [joke, setJoke] = useState("");
  const url = "https://api.api-ninjas.com/v1/dadjokes";

  const config = {
    headers: {
      "X-Api-Key": "qFRwfcoEjiLRvRYhJirWtQ==1JTjQy3ah2MoAPgM",
    },
  };
  console.log();
  useEffect(() => {
    async function fetchData() {
      const response = await fetch(url, config);
      const data = await response.json();
      setJoke(data[0].joke);
    }
    fetchData();
  }, []);
  return <div>{joke}</div>;
}

export default App;
