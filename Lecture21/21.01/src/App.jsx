import { useState } from "react";

const App = () => {
  const [count, setCount] = useState(0);

  useEffect(() => {
    timeout = setTimeout(() => setCount(count + 1));

    return () => clearTimeout(timeout);
  }, [count]);

  return <p>{count}</p>;
};

export default App;
