import { useState } from "react";
import { useEffect } from "react";
import axios from "axios";

const App = () => {
  const [messages, setMessages] = useState([]);
  const [text, setText] = useState({
    text: "",
    user: "",
  });
  const url = "https://buutti-messages-api.azurewebsites.net/api/messages/";
  useEffect(() => {
    const fetchData = async () => {
      const response = await axios.get(url);
      const messageData = response.data;
      setMessages(messageData);
      console.log(messages);
    };
    fetchData();
  }, []);

  const postMessage = async () => {
    const response = await axios.post(url, text);
    setMessages([...messages, response.data]);
  };

  const onTextChange = (event, arg) => {
    setText({ ...text, [arg]: event.target.value });
  };

  return (
    <div className="App">
      <div>
        <fieldset>
          <label>
            Username:
            <input
              type="text"
              value={text.user}
              onChange={(event) => onTextChange(event, "user")}
            />
          </label>
          <label>
            Message:
            <input
              type="text"
              value={text.text}
              onChange={(event) => onTextChange(event, "text")}
            />
          </label>
          <button onClick={postMessage}>Send Message</button>
        </fieldset>
      </div>
      <div>
        {messages.map((message) => {
          return (
            <div key={message.id}>
              {message.text}
              <br />
              -- {message.user}
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default App;
