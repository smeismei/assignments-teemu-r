import { useState } from "react";
import { useEffect } from "react";
import axios from "axios";

const Messages = () => {
  const [messages, setMessages] = useState([]);
  const [text, setText] = useState({
    text: "",
    user: "",
  });
  const url = "https://buutti-messages-api.azurewebsites.net/api/messages/";
  useEffect(() => {
    const fetchData = async () => {
      const response = await axios.get(url);
      const messageData = response.data;
      setMessages(messageData);
      console.log(messages);
    };
    fetchData();
  }, []);

  const postMessage = async () => {
    const response = await axios.post(url, text);
    setMessages([...messages, response.data]);
  };

  const editMessage = async (id, text) => {
    const askMsg = await prompt("Edit message:", text);
    const editurl = url + id;
    const newbody = {
      text: askMsg,
    };
    const response = await axios.patch(editurl, newbody);
    const newMessages = messages.map((message) => {
      return message.id === id ? response.data : message;
    });
    setMessages(newMessages);
  };

  const deleteMessage = async (id) => {
    const delurl = url + id;
    const response = await axios.delete(delurl);
    console.log(response);
    const newMessages = messages.filter((message) => {
      return message.id !== id;
    });
    setMessages(newMessages);
  };

  const onTextChange = (event, arg) => {
    setText({ ...text, [arg]: event.target.value });
  };

  return (
    <div className="Messages">
      <div>
        <fieldset>
          <label>
            Username:
            <input
              type="text"
              value={text.user}
              onChange={(event) => onTextChange(event, "user")}
            />
          </label>
          <label>
            Message:
            <input
              type="text"
              value={text.text}
              onChange={(event) => onTextChange(event, "text")}
            />
          </label>
          <button onClick={postMessage}>Send Message</button>
        </fieldset>
      </div>
      <div>
        {messages.map((message) => {
          return (
            <div key={message.id}>
              {message.text}
              <button onClick={() => editMessage(message.id, message.text)}>
                Edit
              </button>
              <button onClick={() => deleteMessage(message.id)}>Delete</button>
              <br />
              -- {message.user}
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default App;
