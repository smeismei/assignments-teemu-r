import { useState } from "react";
import axios from "axios";

export default function App() {
  const [token, setToken] = useState(null);
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  const submit = async () => {
    const url =
      "https://buutti-messages-api.azurewebsites.net/api/secure/login";
    try {
      const response = await axios.post(url, { username, password });
      console.log(response.data);
      setToken(response.data.token);
    } catch (error) {
      console.log("incorrect username or password");
    }
  };

  if (token === null) {
    return (
      <div>
        <label>
          Username:
          <input
            type="text"
            value={username}
            onChange={(event) => setUsername(event.target.value)}
          />
        </label>
        <label>
          Password:
          <input
            type="text"
            value={password}
            onChange={(event) => setPassword(event.target.value)}
          />
        </label>
        <br />
        <button onClick={submit}>Log In</button>
      </div>
    );
  }
  return <div className="App"></div>;
}
