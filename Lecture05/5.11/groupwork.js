const string = "Rudimentary skills are better than no skills at all";

function checkString(text) {
  const index1 = text.slice(20, 30);
  const noSkill = !index1.includes("skill");
  console.log(noSkill);
  const index2 = text.slice(2, 12);
  const hasMen = index2.includes("men");
  console.log(hasMen);
}
checkString(string);
