//A

const language = "es";

function hello(lan) {
  if (lan === "en") {
    console.log("Hello world!");
  } else if (lan === "fi") {
    console.log("Hei maailma!");
  } else if (lan === "es") {
    console.log("Hola mundo!");
  } else {
    console.log("Unsupported Language");
  }
}

hello(language);

//B

function helloB(lan) {
  if (lan === "en") {
    console.log("Hello world!");
  } else if (lan === "fi") {
    console.log("Hei maailma!");
  } else if (lan === "es") {
    console.log("Hola mundo!");
  } else {
    console.log("Unsupported Language");
  }
}

hello("en");
hello("es");
hello("fi");
