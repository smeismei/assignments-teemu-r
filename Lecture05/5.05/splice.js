const array = ["the", "quick", "brown", "fox"];

console.log(array);
console.log(array[1], array[2]);

array[2] = "grey";
console.log(array[2]);

array.push("over", "lazy", "dog");
console.log(array);
array.unshift("pangram:");

array.splice(5, 0, "jumps");
array.splice(7, 0, "the");
console.log(array);
