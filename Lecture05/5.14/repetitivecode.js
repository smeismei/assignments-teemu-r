function triangleArea(width, length) {
  return (width * length) / 2;
}

const firstTriangleArea = triangleArea(7, 3.5);
const secondTriangleArea = triangleArea(4.3, 6.4);
const thirdTriangleArea = triangleArea(5.5, 5);

function biggestArea(area1, area2, area3) {
  let biggest = -Infinity;
  if (area1 > biggest) {
    biggest = area1;
  }
  if (area2 > biggest) {
    biggest = area2;
  }
  if (area3 > biggest) {
    biggest = area3;
  }

  if (biggest === area1) {
    return "First triangle";
  }
  if (biggest === area2) {
    return "Second triangle";
  }
  if (biggest === area3) {
    return "Third triangle";
  }
}

const areaMax = biggestArea(
  firstTriangleArea,
  secondTriangleArea,
  thirdTriangleArea
);

console.log("Area of the first triangle is", firstTriangleArea);
console.log("Area of the second triangle is", secondTriangleArea);
console.log("Area of the third triangle is", thirdTriangleArea);
console.log(areaMax, "has the biggest area!");
