//named function (my favorite)
function sumTime(num1, num2, num3 = 0) {
  console.log(num1 + num2 + num3);
}
sumTime(1, 4, 5);

//anonymous function
const sumConst = function (num1, num2, num3 = 0) {
  return num1 + num2 + num3;
};
console.log(sumConst(1, 4));

//arrow function
const sumArrow = (num1, num2, num3 = 0) => {
  return num1 + num2 + num3;
};
console.log(sumArrow(1, 4, 5));

//short arrow function
const sumShortArrow = (num1, num2, num3 = 0) => num1 + num2 + num3;

console.log(sumShortArrow(1, 4));
