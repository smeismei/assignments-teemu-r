const beemovie =
  "According to all known laws of aviation, there is no way a bee should be able to fly.";

console.log(beemovie.includes("a"));
console.log(beemovie.startsWith("According"));
console.log(beemovie.endsWith("."));
console.log(beemovie.slice(41));
const split = beemovie.split(" ");
console.log(split[12]);
console.log(split.join(" um "));
console.log(
  beemovie.replace(
    "a bee should be able to fly.",
    "bees are real. They're government drones sent to spy on our flowers."
  )
);
