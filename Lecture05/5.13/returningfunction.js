function minimum(num1, num2, num3) {
  let smallest = +Infinity;
  if (num1 < smallest) {
    smallest = num1;
  }
  if (num2 < smallest) {
    smallest = num2;
  }
  if (num3 < smallest) {
    smallest = num3;
  }
  return smallest;
}

const smallestNumber = minimum(40, 70, 30);
console.log(smallestNumber);

//Alternative, using Math.min

function minimum(num1, num2, num3) {
  return Math.min(num1, num2, num3);
}

console.log(minimum(30, 5, 10));
