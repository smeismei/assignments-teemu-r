//made function inclusive to max and min number because i prefered it
function randomNumber(min, max) {
  const minInt = Math.ceil(min);
  const maxInt = Math.floor(max);
  return Math.floor(Math.random() * (maxInt - minInt) + 1 + minInt);
}

//different variables for different numbers
const number1To10 = randomNumber(1, 10);
const number1To100 = randomNumber(1, 100);
const number1To100000 = randomNumber(1, 100000);

//print it like it's the lottery!
console.log("Today's random number from 1 to 10 is..", number1To10);
console.log("Today's random number from 10 to 100 is..", number1To100);
console.log("Today's random number from 100 to 100000 is..", number1To100000);
