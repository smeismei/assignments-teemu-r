const arr = [
  "banaani",
  "omena",
  "mandariini",
  "appelsiini",
  "kurkku",
  "tomaatti",
  "peruna",
];
console.log(arr[2]);
console.log(arr[4]);
console.log(arr.length);

console.log("Printing all fruit before adding onions..");
arr.sort();
for (let i = 0; i < arr.length; i++) {
  console.log(arr[i]);
}

console.log("Printing all fruit after adding onions.");
arr.push("sipuli");
arr.sort();
for (let i = 0; i < arr.length; i++) {
  console.log(arr[i]);
}

console.log("Removing first item in array..");
arr.shift();
for (let i = 0; i < arr.length; i++) {
  console.log(arr[i]);
}
