const AreWeReal = false;
const AreWeDreaming = true;

if (AreWeDreaming && AreWeReal) {
  console.log("Both are true.");
} else if (AreWeReal) {
  console.log("First is true, second is false.");
} else if (AreWeDreaming) {
  console.log("Second is true, first is false.");
} else {
  console.log("Both are false.");
}
