const p1income = 239478;
const p2income = 40657;
const exponent = 0.9;
const difference = p1income - p2income;
const newIncome1 = p1income ** exponent;
const newIncome2 = p2income ** exponent;

console.log("Difference in player income is ", difference);
console.log(
  "After exponentation the difference becomes ",
  newIncome1 - newIncome2
);
