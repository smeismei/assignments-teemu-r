const person1Age = 15;
const person2Age = 24;

const isFirstPersonOlder = person1Age > person2Age;
console.log(isFirstPersonOlder);

//1. code above prints "false"
//2. isFirstPersonOlder is a boolean
