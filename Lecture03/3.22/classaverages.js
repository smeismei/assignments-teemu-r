const firstClass = 9 + 6 + 9;
const secondClass = 7 + 10 + 5;

const classaverage = (firstClass + secondClass) / 6;
const class1average = firstClass / 3;
const class2average = secondClass / 3;

console.log("The average of both classes was ", classaverage.toFixed(2));
if (class1average > class2average) {
  console.log(
    "Class one had a higher average score with ",
    class1average.toFixed(2)
  );
} else if (class2average > class1average) {
  console.log(
    "Class two had a higher average score with ",
    class2average.toFixed(2)
  );
} else {
  console.log("Both classes are equal.");
}
