const candy = 100;
const remaining = candy % 6;

console.log("i'm stealing ", remaining, " candy");
console.log(
  "and everyone gets ",
  (candy - remaining) / 6,
  " candy (including myself..)"
);
