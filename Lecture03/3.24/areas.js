const inputs = process.argv;
const length = inputs[2];
const width = inputs[3];
const rectangle = length * width;
const triangle = (length * width) / 2;

console.log(
  "A rectangle with a width of ",
  width,
  " and a length of ",
  length,
  " has an area of ",
  rectangle
);
console.log(
  "A triangle with a width of ",
  width,
  " and a length of ",
  length,
  " has an area of ",
  triangle
);
