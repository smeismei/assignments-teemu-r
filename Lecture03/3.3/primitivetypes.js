let numberVar = 25;
let stringVar = "hello, i am a string.";
let booleanVar = false;
let nullVar = null;
let undefinedVar = undefined;
let bigIntVar = 3224n;
let symbolVar = Symbol();

console.log(typeof numberVar);
console.log(typeof stringVar);
console.log(typeof booleanVar);
console.log(typeof nullVar);
console.log(typeof undefinedVar);
console.log(typeof bigIntVar);
console.log(typeof symbolVar);

console.log(
  "> The type of variable that has no assigned value is 'undefined' or 'null', respectively."
);
