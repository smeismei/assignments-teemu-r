const a = 5;
const b = 6;

console.log("Are the numbers the same?", a === b);
console.log("Are they not the same?", a !== b);
console.log("Is A larger than B?", a > b);
console.log("Is A smaller than B?", a < b);
console.log("Is A larger or equal to B?", a >= b);
console.log("Is A smaller or equal to B?", a <= b);
