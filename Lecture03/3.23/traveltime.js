const distance = 2650;
const speed = 320;

const time = distance / speed;
const hours = parseInt(Number(time));
const minutes = Math.round((Number(time) - hours) * 60);

console.log(
  "A distance of ",
  distance,
  " kilometers at a speed of ",
  speed,
  "km/h would take ",
  hours,
  " hours and ",
  minutes,
  " minutes."
);
