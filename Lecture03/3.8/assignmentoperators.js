let n = 4;
console.log("*The original number is ", n, "*");
console.log("*Let's make some changes!*");

n += 5;
console.log("After adding 5, the number is", n);
n *= 2;
console.log("Then multiplied by 2, it is", n);
n -= 5;
console.log("Now substract 5, and the number is", n);
n /= 2;
console.log("And finally, after dividing by 2, the number is", n);
