const a = 5;
const b = 5;

if (a > b) {
  console.log("A is larger than B");
} else if (a < b) {
  console.log("B is larger than A");
} else if (a === b) {
  console.log("A and B are equal");
} else {
  console.log("Are you sure you've given me numbers?");
}
