let variable1 = "hei, olen var1s";
let variable2 = "hei, olen var2s"; //was const but i can't stand errors
let emptyVariable;

console.log(variable1, variable2);
console.log(emptyVariable); //printing an empty variable returns "undefined"

variable1 = "moi, olen muuttunut var1s";
variable2 = "moi, olen muuttumaton !"; //this won't run. cannot assign new value to constant variable.
emptyVariable = "i am valuable!";

console.log(typeof (variable1, variable2));
console.log(emptyVariable);

//we leave it unfixed, them's the assignment!
