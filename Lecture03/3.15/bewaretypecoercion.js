let number;
const result1 = 10 + number;

number = null;
const result2 = 10 + number;

//because "number" isn't assigned a value, it is seen as a boolean
//the code reads it as a boolean and returns NaN as it cannot add 10 to a boolean value.
//in the second example, null is assigned to number, which makes it an "empty" variable.
//this allows the code to ignore it and only count the 10 it's already given.

console.log(result1, result2);
// NaN 10

const a = true;
const b = false;

const c = a + b;
const d = 10 + a;
const e = 10 + b;

//c,d and e return the values 1, 11 and 10.
//c is 1 because the boolean values true and false are 1 and 0
//d is 11 because a is true, which translates to 1
//e is 10 because b is false, which translates to 0

console.log(c, d, e);
