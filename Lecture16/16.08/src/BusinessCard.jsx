function BusinessCard() {
  return (
    <div className="Card">
      <span className="Company">KittyKat</span>
      <span className="Slogan">
        Because your cat might want a Kit Kat, too.
      </span>
      <span className="User">Richard M. Eow</span>
      <span className="Email">rich@kitty.co</span>
      <span className="Phone">+44 506 7745</span>
    </div>
  );
}

export default BusinessCard;
