import Board from "./Board";

function Wall() {
  return (
    <div className="Wall">
      <Board />
      <Board />
      <Board />
    </div>
  );
}

export default Wall;
