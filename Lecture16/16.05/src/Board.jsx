import Target from "./Target";

function Board() {
  return (
    <div className="Board">
      <Target />
      <Target />
      <Target />
      <Target />
    </div>
  );
}

export default Board;
