function Target() {
  return (
    <div className="Target">
      <div className="Target Yellow">
        <div className="Target Blue">
          <div className="Target Red"></div>
        </div>
      </div>
    </div>
  );
}

export default Target;
