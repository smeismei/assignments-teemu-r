import RandomImage from "./RandomImage";

function App() {
  return (
    <div>
      <h1>Random Image</h1>
      <RandomImage />
    </div>
  );
}

export default App;
