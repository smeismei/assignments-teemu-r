import image1 from "./assets/orang1.jpg";
import image2 from "./assets/orang2.jpg";
import image3 from "./assets/orang3.png";

function RandomImage() {
  const randInt = Math.floor(Math.random() * 3);
  const imgPaths = [image1, image2, image3];

  return <img src={imgPaths[randInt]}></img>;
}

export default RandomImage;
