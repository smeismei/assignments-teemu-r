const ranks = {
  "Noel Robinson": "private",
  "Darian Klein": "specialist",
  "Milan Harris": "private",
  "Cedar Eddings": "private",
  "Rory Samson": " corporal",
  "Gerry Smith": "sergeant",
};

const register = {
  JanDaniels: "private",
  ReneAckroyd: "sergeant",
  KerryGilliam: "private",
  LowenMarsh: "corporal",
  EllisDonnel: "private",
  RiverKeller: "private",
  RyanBellerman: "corporal",
};

const merger = (ranks, register) => {
  let newRegister = { ...register };

  for (let key in ranks) {
    const fixedKey = key.replaceAll(" ", "");
    newRegister = { ...newRegister, [fixedKey]: ranks[key] };
  }

  return newRegister;
};

const reg = merger(ranks, register);
console.log(reg);
