import cat from "./assets/catPainting.webp";

function CatPainting() {
  return (
    <div className="catPainting">
      <img src={cat} />
      <p>Cat Painting</p>
    </div>
  );
}

export default CatPainting;
