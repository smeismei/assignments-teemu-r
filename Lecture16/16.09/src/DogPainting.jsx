import dog from "./assets/dogPainting.jpg";

function DogPainting() {
  return (
    <div className="dogPainting">
      <img src={dog}></img>
      <p>Dog Painting</p>
    </div>
  );
}

export default DogPainting;
