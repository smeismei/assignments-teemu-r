import CatPainting from "./CatPainting";
import DogPainting from "./DogPainting";
import PigeonPainting from "./PigeonPainting";

function App() {
  return (
    <div className="Paintings">
      <CatPainting />
      <DogPainting />
      <PigeonPainting />
    </div>
  );
}

export default App;
