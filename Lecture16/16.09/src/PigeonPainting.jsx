import pigeon from "./assets/pigeonPainting.jpg";

function PigeonPainting() {
  return (
    <div className="pigeonPainting">
      <img src={pigeon}></img>
      <p>Pigeon Painting</p>
    </div>
  );
}

export default PigeonPainting;
