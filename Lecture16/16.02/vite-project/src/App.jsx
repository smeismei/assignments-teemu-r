import cat from "./assets/kittycat.jpg"
import catOnClick from "./assets/orang.jpg"

function App() {

  return (
    <div className="App">
      About the simplest React app there is.
      <br></br>
      <img
      id="kitty" 
      src={cat}>
      </img>
    </div>
  )
}

export default App