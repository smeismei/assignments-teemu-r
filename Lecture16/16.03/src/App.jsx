import gus from "./assets/gus.webp"
function App() {
const name = `Gustavo "Gus" Fring`
const age = 51
const weight = 86
const height = 1.77

const bmi = weight/(height * height)

return (
  <div>
  <h1>SUBJECT NAME: {name}</h1>
  <p>SUBJECT AGE: {age} years</p>
  <p>SUBJECT WEIGHT: {weight} Kilograms</p>
  <p>SUBJECT HEIGHT: {height} Meters</p>
  <h3>SUBJECT BMI: {bmi.toFixed(2)}</h3>
  <img src={gus}></img>
  </div>

)
}
export default App