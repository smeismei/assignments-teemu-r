function randomNumber() {
  const randomNum = Math.floor(Math.random() * 100) + 1
  return randomNum
}

function App () {
return (
  <div>
  <h1>Random Number Generator</h1>
  <p>Your Random Number is: {randomNumber()} </p>
  </div>
)
}

export default App