import Row from "./Row";

function Wall() {
  return (
    <div className="Wall">
      <Row />
      <Row />
      <Row />
      <Row />
      <Row />
      <Row />
      <Row />
      <Row />
      <Row />
      <Row />
    </div>
  );
}

export default Wall;
