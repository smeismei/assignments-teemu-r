import Brick from "./Brick";

function Row() {
  return (
    <div className="Row">
      <Brick />
      <Brick />
      <Brick />
      <Brick />
      <Brick />
      <Brick />
      <Brick />
      <Brick />
      <Brick />
      <Brick />
    </div>
  );
}

export default Row;
