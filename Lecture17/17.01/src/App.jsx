import Instrument from "./Instrument";
import violinImg from "./assets/violin.webp";
import guitarImg from "./assets/acousticguitar.webp";
import recorderImg from "./assets/recorder.jpg";

function App() {
  return (
    <div className="divContainer">
      <div className="InstrumentDiv">
        <Instrument name={"Violin"} image={violinImg} price={"400€"} />
        <Instrument name={"Acoustic Guitar"} image={guitarImg} price={"160€"} />
        <Instrument name={"Recorder"} image={recorderImg} price={"20€"} />
      </div>
    </div>
  );
}

export default App;
