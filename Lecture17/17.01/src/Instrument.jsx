function Instrument(props) {
  const { name, image, price } = props;
  return (
    <div className="Instrument">
      <p>{name}</p>
      <img src={image} />
      <p>{price}</p>
    </div>
  );
}

export default Instrument;
