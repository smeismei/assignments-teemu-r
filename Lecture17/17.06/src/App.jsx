import "./App.css";

function App() {
  function logNumber(number) {
    console.log(`User clicked button ${number} at ${new Date().toISOString()}`);
  }
  return (
    <div className="ButtonCentral">
      <button onClick={() => logNumber(1)}>HEYYYYYYYYYYY</button>
      <button onClick={() => logNumber(2)}>HIIIIIIIIIII</button>
    </div>
  );
}

export default App;
