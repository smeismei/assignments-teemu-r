function ListComponent() {
  const namelist = ["Ari", "Jari", "Kari", "Sari", "Mari", "Sakari", "Jouko"];

  const nameListJSX = namelist.map((name, i) =>
    i % 2 === 0 ? (
      <div>
        <b>{name}</b>
      </div>
    ) : (
      <div>
        <i>{name}</i>
      </div>
    )
  );

  return { nameListJSX };
}

export default ListComponent;
