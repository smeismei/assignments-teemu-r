function Star(props) {
  const { name, image, knownFor } = props;

  return (
    <div className="Star">
      <h1>{name}</h1>
      <img src={image}></img>
      <ul>
        {knownFor.map((movie) => (
          <li>{movie}</li>
        ))}
      </ul>
    </div>
  );
}

export default Star;
