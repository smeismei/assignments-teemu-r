import Star from "./Star";
import tomhanks from "./assets/tomhanks.webp";
import samuelljackson from "./assets/samueljackson.webp";
import leonardodicaprio from "./assets/leonardodicaprio.jpg";

function App() {
  const starsList = [
    {
      name: "Tom Hanks",
      image: tomhanks,
      knownFor: [
        "Forrest Gump",
        "Cast Away",
        "Apollo 13",
        "The Green Mile",
        "The Terminal",
      ],
    },
    {
      name: "Samuel L. Jackson",
      image: samuelljackson,
      knownFor: [
        "Pulp Fiction",
        "Django Unchained",
        "The Avengers Franchise",
        "Star Wars: The Phantom Menace",
        "Snakes on a Plane",
      ],
    },
    {
      name: "Leonardo DiCaprio",
      image: leonardodicaprio,
      knownFor: [
        "The Wolf of Wall Street",
        "Inception",
        "Titanic",
        "Django Unchained",
        "The Departed",
      ],
    },
  ];

  return (
    <div>
      {starsList.map((star) => (
        <Star name={star.name} image={star.image} knownFor={star.knownFor} />
      ))}
    </div>
  );
}

export default App;
