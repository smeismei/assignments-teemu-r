import Person from "./Person";
import Family from "./Family";
import "./App.css";

function App() {
  const families = [
    {
      surname: "Nielsen",
      members: [
        { name: "Cedar", age: 2 },
        { name: "Reef", age: 24 },
        { name: "Dale", age: 29 },
      ],
    },
    {
      surname: "Hardin",
      members: [
        { name: "Sutton", age: 8 },
        { name: "Greer", age: 14 },
        { name: "Marley", age: 45 },
        { name: "Rory", age: 47 },
      ],
    },
    {
      surname: "Fowler",
      members: [
        { name: "Hollis", age: 56 },
        { name: "Blair", age: 9 },
      ],
    },
  ];
  return (
    <div className="ListOfFamilies">
      <h1>Obituary</h1>
      {families.map((family) => (
        <li>
          <Family surname={family.surname} members={family.members} />
        </li>
      ))}
    </div>
  );
}

export default App;
