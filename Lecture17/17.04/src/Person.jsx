import "./Person.css";

function Person(props) {
  const { name, age } = props;
  return (
    <p className="Person">
      {name} was {age} years old.
    </p>
  );
}

export default Person;
