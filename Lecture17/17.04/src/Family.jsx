import Person from "./Person";
import "./Family.css";

function Family(props) {
  const { surname, members } = props;

  return (
    <div className="Family">
      <h2>The {surname} Family</h2>
      {members.map((member) => (
        <li>
          <Person name={member.name} age={member.age} />
        </li>
      ))}
    </div>
  );
}

export default Family;
