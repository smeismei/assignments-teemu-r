function App() {
  const indexArray = [...Array(10).keys()];
  const correctPin = 4125;
  let counter = 0;
  let userInput = "";
  function PinCheck(num) {
    counter++;
    userInput += num;
    console.log("USERINPUT:", userInput);
    if (counter == 4 && userInput == correctPin) {
      console.log("Correct Pin!");
      userInput = "";
      counter = 0;
    } else if (counter == 4 && userInput !== correctPin) {
      console.log("Incorrect Pin!");
      userInput = "";
      counter = 0;
    }
  }

  return (
    <div className="Keypad">
      {indexArray.map((i) => (
        <button key={i} id={`Pad${i}`} onClick={() => PinCheck(i)}>
          {i}
        </button>
      ))}
    </div>
  );
}

export default App;
