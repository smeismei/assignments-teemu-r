function isPalindrome(string) {
  const reversed = string.split("").reverse().join("");
  return reversed === string;
}
console.log(isPalindrome("saippuakauppias"));
