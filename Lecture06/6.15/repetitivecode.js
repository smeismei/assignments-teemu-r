function checkSentenceVowels(sentence) {
  let totalCount = 0;
  function VowelHelper(vowel) {
    let countOfVowels = 0;
    for (let i = 0; i < sentence.length; i++) {
      if (sentence.charAt(i).toLowerCase() === vowel) {
        countOfVowels++;
      }
    }
    totalCount = totalCount + countOfVowels;
    console.log(vowel.toUpperCase() + " letter count: " + countOfVowels);
  }
  VowelHelper("a");
  VowelHelper("e");
  VowelHelper("i");
  VowelHelper("o");
  VowelHelper("u");
  VowelHelper("y");

  console.log("Total vowel count: " + totalCount);
}

checkSentenceVowels("A wizard's job is to vex chumps quickly in fog.");
