function exponentValueList(exponent, n) {
  let result = exponent;
  if (n <= 0) {
    console.log("number needs to be positive");
    return;
  }
  for (let counter = 0; counter < n; counter++) {
    console.log(result);
    result = result * exponent;
  }
}

exponentValueList(2, 8);
