let number = 1;
let sum = 0;

while (sum <= 10000) {
  console.log(sum);
  sum += number++;
}

console.log("The last number added was", number - 1);
