const array = [1, 3, 4, 7, 11];

function placeNumber(arr, n) {
  for (let i = 0; i < arr.length; i++) {
    if (arr[i] <= n) {
      i--;
      arr.splice(i, 0, n);
      return;
    }
  }
  arr.push(n);
}

placeNumber(array, 9);
console.log(array);
