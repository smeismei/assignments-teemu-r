function helloLoop(n) {
  let cycles = 0;
  let result = 1;
  while (n > cycles) {
    ++cycles;
    result = result * cycles;
  }
  console.log(result);
}

helloLoop(4);
