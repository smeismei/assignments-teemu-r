const commandList = "NNEESSWWCNNEEENNNCEESSSWNNNECEESWWNNNEEEBENNNEEE";

let y = 0;
let x = 0;

for (let i = 0; i < commandList.length; i++) {
  const cmd = commandList.charAt(i);
  if (cmd === "B") {
    break;
  } else if (cmd === "N") {
    y++;
  } else if (cmd === "E") {
    x++;
  } else if (cmd === "S") {
    y--;
  } else if (cmd === "W") {
    x--;
  } else if (cmd === "C") {
    continue;
  }
}
console.log("the value of Y is:", y);
console.log("the value of X is:", x);
