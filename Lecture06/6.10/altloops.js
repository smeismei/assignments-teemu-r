const array = [5, 7, 2, 9, 3, 13, 15, 6, 17, 24];

for (i in array) {
  if (array[i] % 3 === 0) {
    console.log(array[i]);
  }
}
console.log("for in loop done");

for (i of array) {
  if (i % 3 === 0) {
    console.log(i);
  }
}
console.log("for of loop done");
