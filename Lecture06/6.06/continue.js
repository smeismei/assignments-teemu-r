let result = 1;
for (let cycles = 1; cycles <= 10; cycles++) {
  if (cycles % 3 === 0) {
    continue;
  }
  result = result * cycles;
}
console.log(result);
