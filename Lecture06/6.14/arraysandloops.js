const ages = [20, 35, 27, 44, 24, 32];

let sum = 0;

for (let i = 0; i < ages.length; i++) {
  const element = ages[i];
  sum = sum + element;
}
let average = sum / ages.length;
console.log("The sum of all ages is", sum);
console.log("The average age from this array is", average);
