import { useState } from "react";
import bunnyimg from "./assets/bunny.png";

function App() {
  const [isTextVisible, setTextVisible] = useState(false);
  const [isBunnyVisible, setBunnyVisible] = useState(true);

  function HitTheBunny() {
    const randnum = Math.random() * 5 * 1000;
    setBunnyVisible(false);
    setTextVisible(true);
    setTimeout(() => {
      setTextVisible(false);
      setTimeout(() => setBunnyVisible(true), randnum);
    }, 1000);
  }

  return (
    <div className="Game">
      {isBunnyVisible ? (
        <img id="bunny" src={bunnyimg} onClick={HitTheBunny} />
      ) : null}
      {isTextVisible ? <h2>You hit the bunny!</h2> : null}
    </div>
  );
}

export default App;
