import { useState } from "react";

const App = () => {
  const [counter1, setCounter1] = useState(0);

  const onButtonClick1 = () => {
    setCounter1(counter1 + 1);
  };

  return (
    <div className="App">
      <button onClick={onButtonClick1}>{counter1}</button>
    </div>
  );
};
export default App;
