import { useState } from "react";

const App = () => {
  const [counter1, setCounter1] = useState(0);
  const [counter2, setCounter2] = useState(0);
  const [counter3, setCounter3] = useState(0);

  const onButtonClick1 = () => {
    setCounter1(counter1 + 1);
  };

  const onButtonClick2 = () => {
    setCounter2(counter2 + 1);
  };

  const onButtonClick3 = () => {
    setCounter3(counter3 + 1);
  };

  return (
    <div className="App">
      <button onClick={onButtonClick1}>{counter1}</button>
      <button onClick={onButtonClick2}>{counter2}</button>
      <button onClick={onButtonClick3}>{counter3}</button>
      <br />
      {counter1 + counter2 + counter3}
    </div>
  );
};
export default App;
