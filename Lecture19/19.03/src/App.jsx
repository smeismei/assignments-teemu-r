import { useState } from "react";

function App() {
  const [username, setUsername] = useState("");
  const [title, setTitle] = useState("");
  const [review, setReview] = useState("");
  const [selection, setSelection] = useState("Movie Review");
  const [authorDirector, setAuthor] = useState("");
  const [whoMadeIt, setWhoMadeIt] = useState("");

  const onUsernameChange = (event) => {
    setUsername(event.target.value);
  };

  const onTitleChange = (event) => {
    setTitle(event.target.value);
  };

  const onReviewChange = (event) => {
    setReview(event.target.value);
  };

  const onWhoMadeItChange = (event) => {
    setWhoMadeIt(event.target.value);
  };
  const reset = () => {
    setUsername("");
    setTitle("");
    setReview("");
    setWhoMadeIt("");
  };

  const submit = () => {
    const submittedObj = {
      username,
      title,
      review,
      selection,
      whoMadeIt,
      rating,
    };
    console.log(submittedObj);
  };

  const handleAuthChange = (type) => {
    if (type === "movie") {
      setSelection("Movie Review");
      setAuthor("Director");
    } else if (type === "book") {
      setSelection("Book Review");
      setAuthor("Author");
    }
  };

  return (
    <div className="App">
      <div className="Username">
        <label>
          Username:
          <input type="text" value={username} onChange={onUsernameChange} />
        </label>
      </div>
      <div className="RadioButtons">
        <label>
          Movie Review
          <input
            type="radio"
            checked={selection === "Movie Review"}
            onChange={() => handleAuthChange("movie")}
          />
        </label>
        <label>
          Book Review
          <input
            type="radio"
            checked={selection === "Book Review"}
            onChange={() => handleAuthChange("book")}
          />
        </label>
      </div>
      <div className="Title">
        <label>
          Title:
          <input type="text" value={title} onChange={onTitleChange} />
        </label>
        <span>{rating}</span>
      </div>
      <div className="Review">
        <label>
          Review:
          <textarea value={review} onChange={onReviewChange} />
        </label>
      </div>
      <div className="AuthorDirector">
        <label>
          {authorDirector}
          <input type="text" value={whoMadeIt} onChange={onWhoMadeItChange} />
        </label>
      </div>
      <div className="Buttons">
        <button onClick={reset}>Clear</button>
        <button onClick={submit}>Submit</button>
      </div>
    </div>
  );
}

export default App;
