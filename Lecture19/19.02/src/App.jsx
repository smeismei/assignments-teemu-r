import { useState } from "react";

function App() {
  const [username, setUsername] = useState("");
  const [title, setTitle] = useState("");
  const [review, setReview] = useState("");

  const onUsernameChange = (event) => {
    setUsername(event.target.value);
  };

  const onTitleChange = (event) => {
    setTitle(event.target.value);
  };

  const onReviewChange = (event) => {
    setReview(event.target.value);
  };
  const reset = () => {
    setUsername("");
    setTitle("");
    setReview("");
  };

  const submit = () => {
    const submittedObj = {
      username,
      title,
      review,
    };
    console.log(submittedObj);
  };
  return (
    <div className="App">
      <div className="Username">
        <label>
          Username:
          <input type="text" value={username} onChange={onUsernameChange} />
        </label>
      </div>
      <div className="Title">
        <label>
          Title:
          <input type="text" value={title} onChange={onTitleChange} />
        </label>
      </div>
      <div className="Review">
        <label>
          Review:
          <textarea value={review} onChange={onReviewChange} />
        </label>
      </div>
      <div className="Buttons">
        <button onClick={reset}>Clear</button>
        <button onClick={submit}>Submit</button>
      </div>
    </div>
  );
}

export default App;
