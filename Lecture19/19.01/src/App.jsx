import { useState } from "react";

function App() {
  const [username, setUsername] = useState("");

  const onUsernameChange = (event) => {
    setUsername(event.target.value);
  };
  const reset = () => {
    setUsername("");
  };

  const submit = () => {
    console.log(username);
  };
  return (
    <div className="App">
      <div className="Username">
        <label>
          Username:
          <input type="text" value={username} onChange={onUsernameChange} />
        </label>
      </div>
      <div className="Buttons">
        <button onClick={reset}>Clear</button>
        <button onClick={submit}>Submit</button>
      </div>
    </div>
  );
}

export default App;
