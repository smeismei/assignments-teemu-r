import { useState } from "react";

function App() {
  const [formData, setFormData] = useState({
    username: "",
    title: "",
    review: "",
    selection: "Movie Review",
    authorDirector: "Director",
    whoMadeIt: "",
    rating: "1",
    wantReview: false,
    wantRating: false,
  });

  const onFormChange = (event, arg) => {
    setFormData({ ...formData, [arg]: event.target.value });
  };

  const reset = (event) => {
    setUsername("");
    setTitle("");
    setReview("");
    setWhoMadeIt("");
    setRating("1");
    setWantRating(false);
    setWantReview(false);
    setAuthor("Director");
  };

  const submit = (event) => {
    event.preventDefault();
    const submittedObj = formData;
    console.log(submittedObj);
  };

  const handleAuthChange = (type) => {
    if (type === "movie") {
      setFormData({ ...formData, selection: "Movie Review" });
      setFormData({ ...formData, author: "Director" });
    } else if (type === "book") {
      setFormData({ ...formData, selection: "Book Review" });
      setFormData({ ...formData, author: "Author" });
    }
  };

  return (
    <div className="App">
      <form onSubmit={submit} onReset={reset}>
        <div className="Username">
          <label>
            Username:
            <input
              type="text"
              value={formData.username}
              onChange={(event) => onFormChange(event, "username")}
            />
          </label>
        </div>

        <div className="RadioButtons">
          <label>
            Movie Review
            <input
              type="radio"
              checked={formData.selection === "Movie Review"}
              onChange={() => handleAuthChange("movie")}
            />
          </label>

          <label>
            Book Review
            <input
              type="radio"
              checked={formData.selection === "Book Review"}
              onChange={() => handleAuthChange("book")}
            />
          </label>
        </div>

        <div className="optIn">
          <fieldset>
            <legend>Options:</legend>
            <label>
              I want to write a review.
              <input
                type="checkbox"
                checked={formData.wantReview}
                onChange={() =>
                  setFormData({ ...formData, wantReview: !formData.wantReview })
                }
              />
            </label>

            <label>
              I want to give a rating.
              <input
                type="checkbox"
                checked={formData.wantRating}
                onChange={() =>
                  setFormData({ ...formData, wantRating: !formData.wantRating })
                }
              />
            </label>
          </fieldset>
        </div>

        {formData.wantRating > 0 && (
          <div className="Rating">
            <label>
              Rating:
              <input
                type="range"
                min={1.0}
                max={5.0}
                step={0.25}
                value={formData.rating}
                onChange={(event) => onFormChange(event, "rating")}
              />
              {formData.rating} / 5
            </label>
          </div>
        )}

        <div className="Title">
          <label>
            Title:
            <input
              type="text"
              value={formData.title}
              onChange={(event) => onFormChange(event, "title")}
            />
          </label>
        </div>

        {formData.wantReview > 0 && (
          <div className="Review">
            <label>
              Review:
              <textarea
                value={formData.review}
                onChange={(event) => onFormChange(event, "review")}
              />
            </label>
          </div>
        )}

        <div className="AuthorDirector">
          <label>
            {formData.authorDirector}
            <input
              type="text"
              value={FormData.whoMadeIt}
              onChange={(event) => onFormChange(event, "authorDirector")}
            />
          </label>
        </div>

        <div className="Buttons">
          <input type="reset" value="Reset" />
          <input type="submit" value="Submit" />
        </div>
      </form>
    </div>
  );
}

export default App;
